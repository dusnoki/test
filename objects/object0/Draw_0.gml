/// @description Insert description here
// You can write your code in this editor
cur_planet_research_area = 110;
cur_planet_surface_area = 300;
research_point = 0;
repeat(10) {
	if (cur_planet_research_area < cur_planet_surface_area) {
		cur_planet_research_area += 1;
		if cur_planet_research_area < 200 { chance = 20}
		if cur_planet_research_area >= 200 { chance = 50}
		if cur_planet_research_area >= 600 { chance = 100}
		if cur_planet_research_area >= 1500 { chance = 200}
		if cur_planet_research_area >= 10000 { chance = 300}
		if random(chance) < 1 {research_point ++}
	}
}
draw_text(x,y,research_point);

